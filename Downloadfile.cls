    public static String downloadFiles(Id RecordId){
        List<Id> setOfFilesId=new List<Id>();
        List<ContentDocumentLink> listOfFilesId=[SELECT ContentDocumentId, LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId IN (SELECT Id from <object api name> where Id =: RecordId))];
        
        for(ContentDocumentLink contentDoumentVariable:listOfFilesId){
            setOfFilesId.add(contentDoumentVariable.ContentDocumentId);
        }
        for(Id id:setOfFilesId)
        {
        }
        if(!setOfFilesId.isEmpty()) {
            String downLoadUrl = String.format(String.isBlank(URL.getSalesforceBaseUrl().toExternalForm()) ? '/{0}{1}?' : URL.getSalesforceBaseUrl().toExternalForm() + '/{0}{1}?', 
                                               new String[]{'sfc/servlet.shepherd/version/download/', String.join(setOfFilesId, '/')
                                                   }
                                              );
            return downLoadUrl;
        }
        return null;
    }